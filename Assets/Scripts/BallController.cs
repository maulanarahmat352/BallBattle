﻿using UnityEngine;

public class BallController : MonoBehaviour
{
    public GameObject target;
    public GameObject myParent;
    public bool passed = true;
    Rigidbody rb;
    [SerializeField] float speed = 1.5f;
    PlayerManager pm;

    public void SetManager(PlayerManager playerManager)
    {
        pm = playerManager;
    }
    public void SetRecieverToWait(GameObject atr)
    {
        AttackerController atrCon = atr.GetComponent<AttackerController>();
        if (atrCon != null)
            atrCon.wait = true;
        //atr.wait = true;
    }
    public void MoveTo()
    {
        var step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(target.transform.position.x,
            transform.position.y,
            target.transform.position.z), step);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!passed)
        {
            MoveTo();
            if (target == null)
            {
                pm.hadball = false;
                pm.target = gameObject;
                //passed = true;
            }
        }

    }
}
