using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseManager : MonoBehaviour
{
    public Image fadeImage;
    public RectTransform buttonContainer;
    public Text tittleText;
    public GameObject blueObject;
    public GameObject redObject;
    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        CheckWinLoseCondition();
        StartMainMenuTransition();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void CheckWinLoseCondition()
    {
        if (gameManager.playerScore > gameManager.EnemyScore)
        {
            Debug.Log("Blue Win");
            tittleText.text = "Blue Win";
            blueObject.gameObject.SetActive(true);
        }
        if (gameManager.playerScore < gameManager.EnemyScore)
        {
            Debug.Log("Red Win");
            tittleText.text = "Red Win";
            redObject.gameObject.SetActive(true);
        }
        if (gameManager.playerScore == gameManager.EnemyScore)
        {
            Debug.Log("Draw");
            tittleText.text = "Game Draw";

        }
    }
    public void StartMainMenuTransition()
    {
        Sequence sequence = DOTween.Sequence();
        float baseAnimationDuration = 0.25f;
        float totalanimationDuration = 0;

        sequence.Insert(totalanimationDuration, fadeImage.DOFade(1, 0));
        sequence.Insert(totalanimationDuration, tittleText.gameObject.transform.DOScale(0, 0));
        sequence.Insert(totalanimationDuration, buttonContainer.gameObject.transform.DOScale(0, 0));
        sequence.Insert(totalanimationDuration, fadeImage.DOFade(0, baseAnimationDuration * 8).From(1));
        totalanimationDuration += (baseAnimationDuration * 8);
        sequence.Insert(totalanimationDuration, tittleText.gameObject.transform.DOScale(1, baseAnimationDuration * 2).SetEase(Ease.OutBack));
        totalanimationDuration += (baseAnimationDuration * 2);
        sequence.Insert(totalanimationDuration, buttonContainer.gameObject.transform.DOScale(1, baseAnimationDuration * 2).SetEase(Ease.OutBack));
        totalanimationDuration += (baseAnimationDuration * 2);
    }

    public void Rematch()
    {
        gameManager.ResetScoreAndMatch();
    }
}
