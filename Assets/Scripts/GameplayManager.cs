﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    bool endGame = false;
    float timerTemp;

    [SerializeField] float timer = 140.0f;
    [SerializeField] Text timerText;
    [SerializeField] float countDown = 4;
    [SerializeField] Text countDownText;
    [SerializeField] Text WinText;
    [SerializeField] GameObject playerPion;
    [SerializeField] GameObject EnemyPion;
    [SerializeField] GameObject[] PionDataBank = new GameObject[4];


    PlayerManager playerManager;
    PlayerManager enemyManager;

    public Transform enemyZone;
    public Transform playerZone;

    bool EnemyAttack = false;

    public bool pause = false;
    public bool EndGame = false;

    public GameManager gm;

    void timerController()
    {
        timer -= Time.deltaTime;
        timerText.text = "" + (int)timer;
        if (timer <= 0)
        {
            pause = true;
            countdownController();
        }
    }
    void countdownController()
    {
        timerText.gameObject.SetActive(false);
        countDownText.gameObject.SetActive(true);
        countDown -= Time.deltaTime;
        countDownText.text = "Switch position in : " + (int)countDown;
        if (countDown <= 0)
        {

            if (gm.match == 5)
            {
                timerText.gameObject.SetActive(false);
                countDownText.gameObject.SetActive(false);
                StartCoroutine(checkWin());
            }
            else
            {
                timerText.gameObject.SetActive(true);
                countDownText.gameObject.SetActive(false);
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }

    IEnumerator checkWin()
    {
        string won = "";
        Color col = Color.white;
        if (gm.playerScore == gm.EnemyScore)
        {
            won = "Draw";
            col = Color.black;
        }
        if (gm.playerScore > gm.EnemyScore)
        {
            won = "Blue Win";
            col = Color.blue;
        }
        if (gm.playerScore < gm.EnemyScore)
        {
            won = "Red Win";
            col = Color.red;
        }
        WinText.text = won;
        WinText.color = col;
        WinText.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        //change scene
    }
    public void addScore(PlayerManager pm)
    {
        if (pm.isPlayer)
        {
            gm.playerScore += 1;
            pm.ScoreText.text = "Score : " + gm.playerScore;
        }
        if (!pm.isPlayer)
        {
            gm.EnemyScore += 1;
            pm.ScoreText.text = "Score : " + gm.EnemyScore;
        }
    }

    void TouchInput()
    {
        //for (int i = 0; i < Input.touchCount; i++)
        //{
        Vector3 mouseScreenPosition = Input.mousePosition;
        Ray ray = Camera.main.ScreenPointToRay(mouseScreenPosition);
        //Debug.Log(ray);
        if (Input.GetMouseButtonDown(0))//Input.touches[i].phase == TouchPhase.Began)
        {
            if (Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                if (hitInfo.collider.tag == "PlayerZone")
                {
                    Debug.Log("spawn Player");
                    SpawnAtPosition(hitInfo.point, playerPion, playerManager);

                }
                if (hitInfo.collider.tag == "EnemyZone")
                {
                    Debug.Log("spawn Enemy");
                    SpawnAtPosition(hitInfo.point, EnemyPion, enemyManager);
                }
            }
        }
        //}
    }

    void changeCondition(bool state)
    {
        if (state)
            state = false;
        else if (!state)
            state = true;
    }

    void SpawnAtPosition(Vector3 spawnPosition, GameObject pref, PlayerManager manager)
    {
        if (manager.currentEnergy >= manager.cost)
        {
            GameObject obj = Instantiate(pref, new Vector3(spawnPosition.x, spawnPosition.y + pref.transform.position.y, spawnPosition.z), Quaternion.identity);
            manager.playerPion.Add(obj);
            manager.currentEnergy -= manager.cost;
        }
        else
        {

        }
    }

    void CheckWinLose()
    {
        if (gm.match > 2)
        {
            SceneManager.LoadScene("WinLoseScene");
            //Debug.Log("Show Win Lose Condition");
        }
        else
        {
            if (gm.match % 2 == 0)
            {
                playerPion = PionDataBank[3];
                EnemyPion = PionDataBank[2];
                EnemyAttack = true;
                // Debug.Log("genap");
            }
            if (gm.match % 2 == 1)
            {
                playerPion = PionDataBank[0];
                EnemyPion = PionDataBank[1];
                EnemyAttack = false;
                //Debug.Log("ganjil");
            }
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gm.match += 1;
        PlayerManager[] pm;
        pm = new PlayerManager[2];
        pm = FindObjectsOfType<PlayerManager>();
        WinText.gameObject.SetActive(false);
        CheckWinLose();
        for (int i = 0; i < pm.Length; i++)
        {
            if (pm[i].isPlayer)
            {
                playerManager = pm[i];
                playerManager.ScoreText.text = "Score : " + gm.playerScore;
            }
            else
            {
                enemyManager = pm[i];
                enemyManager.ScoreText.text = "Score : " + gm.EnemyScore;
            }
        }
        if (!EnemyAttack)
        {
            playerManager.cost = playerManager.attackerCost;
            enemyManager.cost = enemyManager.defenderCost;
        }
        if (EnemyAttack)
        {
            playerManager.cost = playerManager.defenderCost;
            enemyManager.cost = enemyManager.attackerCost;

        }
        timerTemp = timer;
        countDownText.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!EndGame)
        {
            timerController();

            if (!pause)
            {
                TouchInput();
            }
        }
        if (endGame)
        {
            countdownController();
        }
    }
}
